<?php

namespace Ensi\BackendServicePostsClient;

class BackendServicePostsClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\BackendServicePostsClient\Api\PostsApi',
        '\Ensi\BackendServicePostsClient\Api\UserVotesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\BackendServicePostsClient\Dto\ResponseBodyPagination',
        '\Ensi\BackendServicePostsClient\Dto\UserVoteFillableCreateProperties',
        '\Ensi\BackendServicePostsClient\Dto\UserVoteReadonlyProperties',
        '\Ensi\BackendServicePostsClient\Dto\PostReadonlyProperties',
        '\Ensi\BackendServicePostsClient\Dto\RequestBodyCursorPagination',
        '\Ensi\BackendServicePostsClient\Dto\SearchUserVotesRequest',
        '\Ensi\BackendServicePostsClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\BackendServicePostsClient\Dto\PatchPostRequest',
        '\Ensi\BackendServicePostsClient\Dto\ErrorResponse',
        '\Ensi\BackendServicePostsClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\BackendServicePostsClient\Dto\Post',
        '\Ensi\BackendServicePostsClient\Dto\CreatePostRequest',
        '\Ensi\BackendServicePostsClient\Dto\Error',
        '\Ensi\BackendServicePostsClient\Dto\UserVoteFillableProperties',
        '\Ensi\BackendServicePostsClient\Dto\PatchUserVoteRequest',
        '\Ensi\BackendServicePostsClient\Dto\UserVoteIncludes',
        '\Ensi\BackendServicePostsClient\Dto\ErrorResponse2',
        '\Ensi\BackendServicePostsClient\Dto\UserVoteResponse',
        '\Ensi\BackendServicePostsClient\Dto\RequestBodyPagination',
        '\Ensi\BackendServicePostsClient\Dto\CreateUserVoteRequest',
        '\Ensi\BackendServicePostsClient\Dto\PostFillableProperties',
        '\Ensi\BackendServicePostsClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\BackendServicePostsClient\Dto\EmptyDataResponse',
        '\Ensi\BackendServicePostsClient\Dto\SearchPostsResponse',
        '\Ensi\BackendServicePostsClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\BackendServicePostsClient\Dto\PaginationTypeEnum',
        '\Ensi\BackendServicePostsClient\Dto\SearchPostsRequest',
        '\Ensi\BackendServicePostsClient\Dto\UserVote',
        '\Ensi\BackendServicePostsClient\Dto\PostResponse',
        '\Ensi\BackendServicePostsClient\Dto\ModelInterface',
        '\Ensi\BackendServicePostsClient\Dto\SearchPostsResponseMeta',
        '\Ensi\BackendServicePostsClient\Dto\PostIncludes',
        '\Ensi\BackendServicePostsClient\Dto\SearchUserVotesResponse',
        '\Ensi\BackendServicePostsClient\Dto\PaginationTypeOffsetEnum',
    ];

    /** @var string */
    public static $configuration = '\Ensi\BackendServicePostsClient\Configuration';
}
