# # PostIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_votes** | [**\Ensi\BackendServicePostsClient\Dto\UserVote[]**](UserVote.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


