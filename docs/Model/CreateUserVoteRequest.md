# # CreateUserVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vote** | **int** | Голос пользователя (1 или -1) | [optional] 
**user_id** | **int** | ID пользователя | [optional] 
**post_id** | **int** | ID поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


