# # UserVoteReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID голосования | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата и время создания голосования | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата и время последнего обновления голосования | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


