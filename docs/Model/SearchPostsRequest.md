# # SearchPostsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**object**](.md) |  | [optional] 
**pagination** | [**\Ensi\BackendServicePostsClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 
**sort** | **string[]** |  | [optional] 
**include** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


