# # PostReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID поста | 
**rating** | **int** | Рейтинг поста | [optional] 
**user_id** | **int** | ID автора поста | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата и время создания поста | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата и время последнего обновления поста | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


