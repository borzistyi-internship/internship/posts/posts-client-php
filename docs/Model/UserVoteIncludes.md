# # UserVoteIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post** | [**\Ensi\BackendServicePostsClient\Dto\Post[]**](Post.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


