# # UserVote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID голосования | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата и время создания голосования | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата и время последнего обновления голосования | 
**vote** | **int** | Голос пользователя (1 или -1) | [optional] 
**user_id** | **int** | ID пользователя | [optional] 
**post_id** | **int** | ID поста | [optional] 
**post** | [**\Ensi\BackendServicePostsClient\Dto\Post[]**](Post.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


