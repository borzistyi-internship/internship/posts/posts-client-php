# Ensi\BackendServicePostsClient\PostsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostsApi.md#createPost) | **POST** /posts | Создание объекта типа Post
[**deletePost**](PostsApi.md#deletePost) | **DELETE** /posts/{id} | Удаление объекта типа Post
[**getPost**](PostsApi.md#getPost) | **GET** /posts/{id} | Получение объекта типа Post
[**patchPost**](PostsApi.md#patchPost) | **PATCH** /posts/{id} | Обновления части полей объекта типа Post
[**searchPost**](PostsApi.md#searchPost) | **POST** /posts:search-one | Поиск объекта типа Post
[**searchPosts**](PostsApi.md#searchPosts) | **POST** /posts:search | Поиск объектов типа Post



## createPost

> \Ensi\BackendServicePostsClient\Dto\PostResponse createPost($create_post_request)

Создание объекта типа Post

Создание объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_post_request = new \Ensi\BackendServicePostsClient\Dto\CreatePostRequest(); // \Ensi\BackendServicePostsClient\Dto\CreatePostRequest | 

try {
    $result = $apiInstance->createPost($create_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_post_request** | [**\Ensi\BackendServicePostsClient\Dto\CreatePostRequest**](../Model/CreatePostRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \Ensi\BackendServicePostsClient\Dto\EmptyDataResponse deletePost($id)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPost

> \Ensi\BackendServicePostsClient\Dto\PostResponse getPost($id, $include)

Получение объекта типа Post

Получение объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPost($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->getPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServicePostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \Ensi\BackendServicePostsClient\Dto\PostResponse patchPost($id, $patch_post_request)

Обновления части полей объекта типа Post

Обновления части полей объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_post_request = new \Ensi\BackendServicePostsClient\Dto\PatchPostRequest(); // \Ensi\BackendServicePostsClient\Dto\PatchPostRequest | 

try {
    $result = $apiInstance->patchPost($id, $patch_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_post_request** | [**\Ensi\BackendServicePostsClient\Dto\PatchPostRequest**](../Model/PatchPostRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPost

> \Ensi\BackendServicePostsClient\Dto\PostResponse searchPost($search_posts_request)

Поиск объекта типа Post

Поиск объектов типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \Ensi\BackendServicePostsClient\Dto\SearchPostsRequest(); // \Ensi\BackendServicePostsClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPost($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\Ensi\BackendServicePostsClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \Ensi\BackendServicePostsClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск объектов типа Post

Поиск объектов типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \Ensi\BackendServicePostsClient\Dto\SearchPostsRequest(); // \Ensi\BackendServicePostsClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\Ensi\BackendServicePostsClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

