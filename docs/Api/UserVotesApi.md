# Ensi\BackendServicePostsClient\UserVotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserVote**](UserVotesApi.md#createUserVote) | **POST** /user-votes | Создание объекта типа UserVote
[**deleteUserVote**](UserVotesApi.md#deleteUserVote) | **DELETE** /user-votes/{id} | Удаление объекта типа UserVote
[**patchUserVote**](UserVotesApi.md#patchUserVote) | **PATCH** /user-votes/{id} | Обновления части полей объекта типа UserVote
[**searchUserVotes**](UserVotesApi.md#searchUserVotes) | **POST** /user-votes:search | Поиск объектов типа UserVote



## createUserVote

> \Ensi\BackendServicePostsClient\Dto\UserVoteResponse createUserVote($create_user_vote_request)

Создание объекта типа UserVote

Создание объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\UserVotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_user_vote_request = new \Ensi\BackendServicePostsClient\Dto\CreateUserVoteRequest(); // \Ensi\BackendServicePostsClient\Dto\CreateUserVoteRequest | 

try {
    $result = $apiInstance->createUserVote($create_user_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserVotesApi->createUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_vote_request** | [**\Ensi\BackendServicePostsClient\Dto\CreateUserVoteRequest**](../Model/CreateUserVoteRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\UserVoteResponse**](../Model/UserVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteUserVote

> \Ensi\BackendServicePostsClient\Dto\EmptyDataResponse deleteUserVote($id)

Удаление объекта типа UserVote

Удаление объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\UserVotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteUserVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserVotesApi->deleteUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchUserVote

> \Ensi\BackendServicePostsClient\Dto\UserVoteResponse patchUserVote($id, $patch_user_vote_request)

Обновления части полей объекта типа UserVote

Обновления части полей объекта типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\UserVotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_user_vote_request = new \Ensi\BackendServicePostsClient\Dto\PatchUserVoteRequest(); // \Ensi\BackendServicePostsClient\Dto\PatchUserVoteRequest | 

try {
    $result = $apiInstance->patchUserVote($id, $patch_user_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserVotesApi->patchUserVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_user_vote_request** | [**\Ensi\BackendServicePostsClient\Dto\PatchUserVoteRequest**](../Model/PatchUserVoteRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\UserVoteResponse**](../Model/UserVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUserVotes

> \Ensi\BackendServicePostsClient\Dto\SearchUserVotesResponse searchUserVotes($search_user_votes_request)

Поиск объектов типа UserVote

Поиск объектов типа UserVote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServicePostsClient\Api\UserVotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_user_votes_request = new \Ensi\BackendServicePostsClient\Dto\SearchUserVotesRequest(); // \Ensi\BackendServicePostsClient\Dto\SearchUserVotesRequest | 

try {
    $result = $apiInstance->searchUserVotes($search_user_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserVotesApi->searchUserVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_user_votes_request** | [**\Ensi\BackendServicePostsClient\Dto\SearchUserVotesRequest**](../Model/SearchUserVotesRequest.md)|  |

### Return type

[**\Ensi\BackendServicePostsClient\Dto\SearchUserVotesResponse**](../Model/SearchUserVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

